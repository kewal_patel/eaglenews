from django.conf.urls import url, include
from .views import *
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r'^ad/$', AdList.as_view(), name='ad_list'),
    url(r'^ad/category/$', AdCategoryView.as_view(), name='ad_category'),
    url(r'^ad/location/get/(?P<pk>[0-9]+)/$', AdLocationView.as_view(), name='ad_location'),
    url(r'^ad/location/$', AddLocationView.as_view(), name='add_location'),
    # url(r'^users/login/$',RedirectView.as_view(url='/api-token-auth/login')),
    # url(r'^ad/get/(?P<pk>[0-9]+)/$',AdvertisementDetail.as_view(),name='ad_detail'),
    url(r'^ad/get/(?P<pk>[0-9]+)/$', AdDetail.as_view(), name='ad_detail'),
    url(r'^ad/image/get/(?P<pk>[0-9]+)/$', AdImageDetail.as_view(), name='ad_image_view'),
    url(r'^users/get/(?P<pk>[0-9]+)/$', UserDetail.as_view(), name='user-detail'),
    url(r'^users/$', UserList.as_view()),
    url(r'^users/register/$', UserRegistration.as_view(), name='user_register'),
    url(r'^media_files/upload/', AdImageView.as_view(), name='image_upload'),
    url(r'^ad/category/(?P<pk>[0-9]+)/$', CategoryListView.as_view(), name='categoryView'),

]
