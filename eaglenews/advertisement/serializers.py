from rest_framework import serializers
from .models import *
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password


class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-rest-framework/pull/1268

    Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12]  # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension,)

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension


class AdvertisementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Advertisement
        fields = (
            'id',
            'user',
            'title',
            'description',
            'created'
        )


class AdListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Advertisement
        fields = ('id', 'user', 'title', 'description')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    # ad_user = serializers.PrimaryKeyRelatedField(many=True,queryset=User.objects.all())
    adUser = serializers.HyperlinkedRelatedField(many=True, view_name='ad_detail', read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'password', 'email', 'is_active', 'adUser')


class UserRegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username', 'first_name', 'last_name', 'password', 'email')


class AdImageSerializer(serializers.ModelSerializer):
    image = Base64ImageField(
        max_length=None, use_url=True, )

    class Meta:
        model = AdImages
        fields = ('ad_id', 'image')


class AdImagePathSerializer(serializers.ModelSerializer):
    image = serializers.CharField()

    class Meta:
        model = AdImages
        lookup_field = 'ad_id'
        fields = ('image',)


class AdCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = AdCategory
        fields = ('id', 'category_name')

class AdSerializer(serializers.ModelSerializer):
    # adimage = serializers.HyperlinkedRelatedField(many=True,view_name='ad_image_view', read_only=True,lookup_url_kwarg = "ad_id")
    adimage = AdImagePathSerializer(many=True,read_only=True)

    class Meta:
        model = Ad
        fields = ('id', 'user', 'title', 'description', 'adimage')


class AdLocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdLocation
        fields = ('ad_id', 'latitude', 'longitude')
