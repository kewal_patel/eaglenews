from django.contrib import admin
from .models import Advertisement,Ad,AdImages,AdLocation,AdCategory
# Register your models here.

admin.site.register(Advertisement)
admin.site.register(Ad)
admin.site.register(AdCategory)
admin.site.register(AdLocation)
admin.site.register(AdImages)
