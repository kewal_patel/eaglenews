from django.shortcuts import render
from .models import *
# from .serializers import AdvertisementSerializer,UserSerializer,AdListSerializer
from .serializers import *
from rest_framework.views import APIView
from django.http import Http404
from rest_framework.response import Response
from datetime import datetime
from django.utils import timezone
from rest_framework import status, generics, permissions
from django.contrib.auth.models import User
from .permissions import *
import eaglenews.settings as settings
from rest_framework.parsers import *


# class UserList(generics.ListAPIView):
# 	queryset = User.objects.all()
# 	serializer_class = UserSerializer

class UserList(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsUser,)

    """
    List all users, or create a new user.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserRegistration(generics.CreateAPIView):
    serializer_class = UserRegistrationSerializer

    def perform_create(self, serializer):
        hashed_password = make_password(serializer.validated_data['password'])
        serializer.validated_data['password'] = hashed_password
        user = super(UserRegistration, self).perform_create(serializer)


class UserDetail(generics.RetrieveAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsUser,)

    queryset = User.objects.all()
    serializer_class = UserSerializer


# class AdvertisementList(generics.ListCreateAPIView):

# 	permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

# 	queryset = Advertisement.objects.all()
# 	serializer_class = AdvertisementSerializer

# 	def pre_save(self,obj):
# 		obj.user = self.request.user

class AdList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    queryset = Ad.objects.all()
    serializer_class = AdSerializer

    def pre_save(self, obj):
        obj.user = self.request.user


# class AdvertisementDetail(generics.RetrieveUpdateDestroyAPIView):

# 	permission_classes = (permissions.IsAuthenticatedOrReadOnly,
#                       IsOwnerOrReadOnly,)	

# 	queryset = Advertisement.objects.all()
# 	serializer_class = AdvertisementSerializer

# 	def pre_save(self,obj):
# 		obj.user = self.request.user

class AdDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)

    queryset = Ad.objects.all()
    serializer_class = AdSerializer

    def pre_save(self, obj):
        obj.user = self.request.user


class AdImageDetail(generics.RetrieveUpdateDestroyAPIView):
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly,)
    queryset = AdImages.objects.all()
    serializer_class = AdImagePathSerializer


class AdImageView(APIView):
    permission_classes = (IsOwnerOrReadOnly,)
    parser_classes = (FormParser, MultiPartParser,)

    def post(self, request, format=None):
        serializer = AdImageSerializer(data=self.request.data)
        # try:
        #     import os
        #     adID = request.data['ad_id']
        #     ad_obj = AdImages.objects.get(ad_id=adID)
        #     ad_obj.delete()
        #     path = os.path.join(settings.MEDIA_ROOT,adID)
        #     path += ".jpg"
        #     print path
        #     if os.path.exists(path):
        #         os.remove(path)
        # except AdImages.DoesNotExist:
        #         pass
        if serializer.is_valid():
            file_name = serializer.validated_data['ad_id'].id
            print file_name
            serializer.validated_data['image'].name = "%s.jpg" % (file_name)
            serializer.save()
            return Response("image uploaded successfully", status=status.HTTP_201_CREATED)
        else:
            print "else"
            print serializer.errors
            return Response("Failed to upload", status=status.HTTP_400_BAD_REQUEST)


class AdCategoryView(generics.ListAPIView):
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly,)
    queryset = AdCategory.objects.all()
    serializer_class = AdCategorySerializer


class CategoryListView(generics.ListAPIView):
    serializer_class = AdSerializer

    def get_queryset(self):
        # print dir(self.kwargs)
        category_id = self.kwargs['pk']
        return Ad.objects.filter(category_id=category_id)


# class CategoryDetailView(generics.RetrieveUpdateDestroyAPIView):
#
#     serializer_class = AdCategorySerializer
#     queryset = AdCategory.objects.all()

class AdLocationView(APIView):
    def get_object(self, pk):
        try:
            return Ad.objects.get(pk=pk)
        except Ad.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        print "request", request
        ad_obj = self.get_object(pk)
        ad_location_obj = ad_obj.adlocation.all()[0]
        serializer = AdLocationSerializer(ad_location_obj)
        print serializer.data, '*' * 10
        return Response(serializer.data)


class AddLocationView(APIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)

    def post(self, request, format=None):

        serializer = AdLocationSerializer(data=request.data)
        if serializer.is_valid():
            instance = serializer.save()
            print instance, 'saved adlocation'
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
