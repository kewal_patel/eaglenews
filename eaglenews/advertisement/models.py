from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from model_utils.models import TimeStampedModel
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from eaglenews import settings


# Create your models here.
class Advertisement(TimeStampedModel):
    user = models.ForeignKey(User, related_name='ad_user')
    title = models.CharField(max_length=30)
    description = models.CharField(max_length=300)

    def __unicode__(self):
        return '%s' % self.title


class AdCategory(models.Model):
    category_name = models.CharField(max_length=15)

    def __unicode__(self):
        return '%s' % self.category_name


class Ad(models.Model):
    user = models.ForeignKey(User, related_name="adUser")
    title = models.CharField(max_length=30)
    description = models.CharField(max_length=300)
    category_id = models.ForeignKey(AdCategory, related_name="ad_category")
    ad_created = models.DateField(auto_now_add=True)

    def __unicode__(self):
        return '%s %s %s' % (self.user, self.title, self.ad_created)


class AdImages(models.Model):
    ad_id = models.ForeignKey(Ad, related_name="adimage")
    image = models.ImageField(upload_to=settings.MEDIA_ROOT, blank=True)

    def __unicode__(self):
        return self.image.path


class AdLocation(models.Model):
    ad_id = models.ForeignKey(Ad, related_name="adlocation")
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)

    def __unicode__(self):
        return '%s %s %s %s' % (self.id, self.ad_id, self.latitude, self.longitude)


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
