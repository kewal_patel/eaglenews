# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('advertisement', '0003_auto_20160306_2030'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='advertisement',
            name='date_added',
        ),
    ]
