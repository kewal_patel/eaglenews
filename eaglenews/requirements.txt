Django==1.9.5
django-model-utils==2.4
djangorestframework==3.3.3
Pillow==3.2.0
psycopg2==2.6.1
six==1.10.0
virtualenv==15.0.1
